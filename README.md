# ivo-on-a-mac

Misc notes, utilities and configs from someone that doesn't feel very at home on a mac

* <./Portuguese-Linux keyboard layout/> Portuguese keyboard layout to match what's used in Linux (and Windows) laptops. Created using the awesome [Ukelele](https://software.sil.org/ukelele/) utility.